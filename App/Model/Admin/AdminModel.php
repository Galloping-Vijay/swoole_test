<?php
/**
 * Description:
 * Created by PhpStorm.
 * User: Vijay
 * Date: 2020/3/4
 * Time: 21:04
 */

namespace App\Model\Admin;

use EasySwoole\ORM\AbstractModel;

/**
 * Class AdminModel
 * @package App\Model\Admin
 * @property $adminId
 */
class AdminModel extends AbstractModel
{
    protected $tableName = 'admin_list';

    protected $primaryKey = 'adminId';

    /**
     * Description:
     * User: Vijay
     * Date: 2020/3/4
     * Time: 21:07
     * @param int $page
     * @param string|null $keyword
     * @param int $pageSize
     * @return array
     * @throws \EasySwoole\ORM\Exception\Exception
     * @throws \Throwable
     */
    public function getAll(int $page = 1, string $keyword = null, int $pageSize = 10): array
    {
        $where = [];
        if (!empty($keyword)) {
            $where['adminAccount'] = ['%' . $keyword . '%', 'like'];
        }
        $list = $this->limit($pageSize * ($page - 1), $pageSize)->order($this->primaryKey, 'DESC')->withTotalCount()->all($where);
        $total = $this->lastQueryResult()->getTotalCount();
        return ['total' => $total, 'list' => $list];
    }

    /**
     * Description:登录成功后请返回更新后的bean
     * User: Vijay
     * Date: 2020/3/4
     * Time: 21:08
     * @return AdminModel|null
     * @throws \EasySwoole\Mysqli\Exception\Exception
     * @throws \EasySwoole\ORM\Exception\Exception
     * @throws \Throwable
     */
    public function login(): ?AdminModel
    {
        $info = $this->get(['adminAccount' => $this->adminAccount, 'adminPassword' => $this->adminPassword]);
        return $info;
    }

    /**
     * Description:以account进行查询
     * User: Vijay
     * Date: 2020/3/4
     * Time: 21:08
     * @param string $field
     * @return AdminModel|null
     * @throws \EasySwoole\Mysqli\Exception\Exception
     * @throws \EasySwoole\ORM\Exception\Exception
     * @throws \Throwable
     */
    public function accountExist($field = '*'): ?AdminModel
    {
        $info = $this->field($field)->get(['adminAccount' => $this->adminAccount]);
        return $info;
    }

    /**
     * Description:
     * User: Vijay
     * Date: 2020/3/4
     * Time: 21:08
     * @param string $field
     * @return AdminModel|null
     * @throws \EasySwoole\Mysqli\Exception\Exception
     * @throws \EasySwoole\ORM\Exception\Exception
     * @throws \Throwable
     */
    public function getOneBySession($field = '*'): ?AdminModel
    {
        $info = $this->field($field)->get(['adminSession' => $this->adminSession]);
        return $info;
    }

    /**
     * Description:
     * User: Vijay
     * Date: 2020/3/4
     * Time: 21:08
     * @return bool
     * @throws \EasySwoole\Mysqli\Exception\Exception
     * @throws \EasySwoole\ORM\Exception\Exception
     * @throws \Throwable
     */
    public function logout()
    {
        return $this->update(['adminSession' => '']);
    }
}