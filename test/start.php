<?php
// +----------------------------------------------------------------------
// | ${NAME}.
// +----------------------------------------------------------------------
// | Copyright (c) 2018 http://www.yuemeet.com, All rights reserved.
// +----------------------------------------------------------------------
// | Author: vijay <1937832819@qq.com> 2018-10-23
// +----------------------------------------------------------------------

//ini_set('display_errors', 'on');
//判断当前环境
if (strpos(strtolower(PHP_OS), 'win') === 0) {
    exit("start.php not support windows, please use start_for_win.bat\n");
}

// 检查扩展
/*if (!extension_loaded('pcntl')) {
    exit("Please install pcntl extension.\n");
}

if (!extension_loaded('posix')) {
    exit("Please install posix extension\n");
}*/

// 加载所有start.php，以便启动所有服务
foreach (glob(__DIR__ . '/server/start_*.php') as $start_file) {
    require_once $start_file;
}